﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            //пустой
            //пустой 
        }


        public int[] SwapItems(int a, int b)
        {
            return new int[] { b, a };
        }

        public int GetMinimumValue(int[] input)
        {
            int min = input[0];
            for (int i = 1; i < input.Length; i++)
                if (min > input[i])
                    min = input[i];
            return min;
        }

    }
}

